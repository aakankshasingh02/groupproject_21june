class Ranking :
    def __init__(self): 
        self.names = []
        self.marks = []
        self.relations = []
    
    def student_marks(self, filename: str) -> list :
        with open(filename) as file:
            for row in file.readlines():
                if row == '\n': continue
                row = row.strip().split(' ')
                self.names += [row[0]] 
                self.marks += [list(map(int,row[1:]))]
        self.making_relation()
        
    def making_relation(self):
        num = 0
        for row1, row2 in zip(self.marks, self.marks[1:]) :
            if all([m1>m2 for m1,m2 in zip(row1, row2)]) == True:
                self.relations += [f"{self.names[num]} > {self.names[num+1]}"]
                
            if all([m1<m2 for m1,m2 in zip(row1, row2)]) == True:
                self.relations += [f"{self.names[num]} < {self.names[num+1]}"]
            
            num += 1

                
                
                
                

q = Ranking()
q.student_marks("studentRanking.txt")
print(q.names)
print(q.marks)
print(q.relations)