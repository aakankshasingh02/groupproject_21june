class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks

    def __str__(self):
        return self.name

students = [
    Student('A', [12, 14, 16]),
    Student('B', [5, 6, 7]),
    Student('C', [17, 20, 23]),
    Student('D', [2, 40, 12]),
    Student('E', [3, 41, 13]),
    Student('F', [7, 8, 9]),
    Student('G', [4, 5, 6]),
]

students.sort(key=lambda self: self.marks)
print(' < '.join(map(str, students)))